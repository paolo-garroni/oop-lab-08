package it.unibo.oop.lab.advanced;

import javax.swing.JOptionPane;

public class DrawNumberViewConsole extends DrawNumberViewImpl {
	
	public DrawNumberViewConsole() {
		super();
		this.start();
	}
	
    @Override
    public void result(final DrawResult res) {
        switch (res) {
        case YOURS_HIGH:
        case YOURS_LOW:
            plainMessage(res.getDescription());
            System.out.println(res.getDescription());
            return;
        case YOU_WON:
            plainMessage(res.getDescription() + NEW_GAME);
            System.out.println(res.getDescription());
            break;
        case YOU_LOST:
            JOptionPane.showMessageDialog(frame, res.getDescription() + NEW_GAME, "Lost", JOptionPane.WARNING_MESSAGE);
            System.out.println(res.getDescription());
            break;
        default:
            throw new IllegalStateException("Unexpected result: " + res);
        }
        observer.resetGame();
    }
    
    @Override
    public void numberIncorrect() {
    	super.numberIncorrect();
    	System.out.println("Incorrect Number.. try again");
    }
}
