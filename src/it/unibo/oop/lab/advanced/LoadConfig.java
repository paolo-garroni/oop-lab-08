package it.unibo.oop.lab.advanced;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class LoadConfig {
	
	Map<String, Integer> options = new HashMap<>();
	
    public Map<String, Integer> getOptions() throws IOException {
        BufferedReader config = new BufferedReader(new FileReader("res/config.yml"));
        String line;
        while ((line = config.readLine()) != null) {    
        	StringTokenizer st = new StringTokenizer(line, ": ");
        	while (st.hasMoreTokens()) {
        		String property = st.nextToken();
        		if (property.compareToIgnoreCase("MINIMUM") == 0) {
        			this.options.put("MINIMUM", Integer.valueOf(st.nextToken()));
        		} else if (property.compareToIgnoreCase("MAXIMUM") == 0) {
        			this.options.put("MAXIMUM", Integer.valueOf(st.nextToken()));
        		} else if (property.compareToIgnoreCase("ATTEMPTS") == 0) {
        			this.options.put("ATTEMPTS", Integer.valueOf(st.nextToken()));
        		}
        	}
        }
        config.close();
        return this.options;
    }
    
}
