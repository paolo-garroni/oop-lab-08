package it.unibo.oop.lab.advanced;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    private static int min;
    private static int max;
    private static int attempts;
    private final DrawNumber model;
    private final List<DrawNumberView> views;


    public DrawNumberApp() {
    	try {
    		Map<String, Integer> options = new LoadConfig().getOptions();
    		DrawNumberApp.max = options.get("MAXIMUM");
    		DrawNumberApp.min = options.get("MINIMUM");
    		DrawNumberApp.attempts = options.get("ATTEMPTS");
    	} catch (IOException e) {
			throw new IllegalStateException(e);
		}
        this.model = new DrawNumberImpl(min, max, attempts);
        this.views = new ArrayList<>();
        this.views.add(new DrawNumberViewLog());
        this.views.add(new DrawNumberViewConsole());
        this.views.forEach(v -> v.setObserver(this));       
    }
    
    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            this.views.forEach(v -> v.result(result));
        } catch (IllegalArgumentException e) {
            this.views.forEach(v -> v.numberIncorrect());
        } catch (AttemptsLimitReachedException e) {
            this.views.forEach(v -> v.limitsReached());
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        new DrawNumberApp();
    }

}
