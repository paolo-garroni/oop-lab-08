package it.unibo.oop.lab.advanced;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import javax.swing.JOptionPane;

public class DrawNumberViewLog extends DrawNumberViewImpl {
		
	public DrawNumberViewLog() {
		super();
		this.start();
	}

    @Override
    public void result(final DrawResult res) {
        switch (res) {
        case YOURS_HIGH:
        case YOURS_LOW:
            plainMessage(res.getDescription());
            writeOnLogFile(res.getDescription());
            return;
        case YOU_WON:
            plainMessage(res.getDescription() + NEW_GAME);
            writeOnLogFile(res.getDescription());            break;
        case YOU_LOST:
            JOptionPane.showMessageDialog(frame, res.getDescription() + NEW_GAME, "Lost", JOptionPane.WARNING_MESSAGE);
            writeOnLogFile(res.getDescription());            break;
        default:
            throw new IllegalStateException("Unexpected result: " + res);
        }
        observer.resetGame();
    }
    
    @Override
    public void numberIncorrect() {
    	super.numberIncorrect();
    	writeOnLogFile("Incorrect Number.. try again");
    }
    
    private void writeOnLogFile(final String message) {
    	try {
			Files.write(Paths.get("output.log"), "\n".concat(message).getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
    }
}
