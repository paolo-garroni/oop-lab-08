package it.unibo.oop.lab.mvcio;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.*;

import it.unibo.oop.lab.mvcio2.SimpleGUIWithFileChooser;

/**
 * A very simple program using a graphical interface.
 * 
 */
public class SimpleGUI {

    private final JFrame frame = new JFrame();
    private final Controller controller = new Controller();
    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextArea with a button "Save" right
     * below (see "ex02.png" for the expected result). SUGGESTION: Use a JPanel with
     * BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The program asks the controller to save the file if the button "Save" gets
     * pressed.
     * 
     * Use "ex02.png" (in the res directory) to verify the expected aspect.
     */

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {
        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        // My code:
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter( ) {
        	public void windowClosing(WindowEvent we) {
        		int option = JOptionPane.showConfirmDialog(
        				frame, "Are you sure you want to exit?");
				if (option == JOptionPane.OK_OPTION) {
					System.exit(0);
				}
        	}
        });
        JPanel mainPanel = new JPanel(new BorderLayout());
        JTextArea textArea = new JTextArea();
        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					SimpleGUI.this.controller.writeFileContent(textArea.getText());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
        mainPanel.add(saveButton, BorderLayout.SOUTH);
        mainPanel.add(textArea, BorderLayout.CENTER);
        frame.add(mainPanel);
    }
    
    public void startGui() {
    	frame.setVisible(true);
    }
    
    public static void main(String args[]) {
    	SimpleGUIWithFileChooser myGUI = new SimpleGUIWithFileChooser();
    	myGUI.startGui();
    }

}
