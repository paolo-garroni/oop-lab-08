package it.unibo.oop.lab.mvcio;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Objects;

/**
 * 
 */
public class Controller {

    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     * 
     * 2) A method for getting the current File
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */
	
	private File file;
	private Path path;
	
	private static final String DEFAULT_PATHNAME = System.getProperty("user.home") 
			+ System.getProperty("file.separator") + "output.txt";
	
	public Controller() {
		this.file = new File(Controller.DEFAULT_PATHNAME);
		path = file.toPath();
	}
	
	public void setFile(File file) {
		this.file = Objects.requireNonNull(file);
		this.path = file.toPath();
	}
	
	public File getFile() {
		return this.file;
	}
	
	public String getFilePath() {
		return this.file.getPath();
	}
	
	public void writeFileContent(String content) throws IOException {
		try(BufferedWriter writer = Files.newBufferedWriter(this.path, StandardCharsets.UTF_8, 
				StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {
			writer.append(content);
		}
	
	}

}
