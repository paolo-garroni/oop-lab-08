package it.unibo.oop.lab.mvcio2;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.*;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser extends SimpleGUI {

	 private final JFrame frame = new JFrame();
	 private final Controller controller = new Controller();
	 
    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

	 public SimpleGUIWithFileChooser() {
		 /*
		     * Make the frame half the resolution of the screen. This very method is
		     * enough for a single screen setup. In case of multiple monitors, the
		     * primary is selected.
		     * 
		     * In order to deal coherently with multimonitor setups, other
		     * facilities exist (see the Java documentation about this issue). It is
		     * MUCH better than manually specify the size of a window in pixel: it
		     * takes into account the current resolution.
		     */
		    final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		    final int sw = (int) screen.getWidth();
		    final int sh = (int) screen.getHeight();
		    frame.setSize(sw / 2, sh / 2);
		    /*
		     * Instead of appearing at (0,0), upper left corner of the screen, this
		     * flag makes the OS window manager take care of the default positioning
		     * on screen. Results may vary, but it is generally the best choice.
		     */
		    frame.setLocationByPlatform(true);
		    // My code:
		    frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		    frame.addWindowListener(new WindowAdapter( ) {
		    	public void windowClosing(WindowEvent we) {
		    		int option = JOptionPane.showConfirmDialog(
		    				frame, "Are you sure you want to exit?");
					if (option == JOptionPane.OK_OPTION) {
						System.exit(0);
					}
		    	}
		    });
		    JPanel mainPanel = new JPanel(new BorderLayout());
		    JTextArea textArea = new JTextArea();
		    JButton saveButton = new JButton("Save");
		    saveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						SimpleGUIWithFileChooser.this.controller.writeFileContent(textArea.getText());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			});
		    JPanel upperPanel = new JPanel(new BorderLayout());
		    JTextField browseTextField = new JTextField(this.controller.getFilePath());
		    browseTextField.setEditable(false);
		    JButton browseButton = new JButton("Browse");
		    browseButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JFileChooser fileChooser = new JFileChooser(controller.getFile());
					int returnVal = fileChooser.showOpenDialog(frame);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						controller.setFile(fileChooser.getSelectedFile());
						browseTextField.setText(SimpleGUIWithFileChooser.this.controller.getFilePath());
					} else if (returnVal == JFileChooser.CANCEL_OPTION) {
						// do nothing
					} else {
						JOptionPane.showMessageDialog(frame, "Ooops! An error has occurred!");
					}
				}
			});
		    upperPanel.add(browseButton, BorderLayout.LINE_END);
		    upperPanel.add(browseTextField, BorderLayout.CENTER);
		    mainPanel.add(upperPanel, BorderLayout.NORTH);
		    mainPanel.add(saveButton, BorderLayout.SOUTH);
		    mainPanel.add(textArea, BorderLayout.CENTER);
		    frame.add(mainPanel);
	 }
	 
	 public void startGui() {
	    	frame.setVisible(true);
	 }
	    
	 public static void main(String args[]) {
	  	SimpleGUIWithFileChooser gui = new SimpleGUIWithFileChooser();
	   	gui.startGui();
	 }
}




    

