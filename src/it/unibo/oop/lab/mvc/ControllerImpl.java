package it.unibo.oop.lab.mvc;

import java.util.*;

public class ControllerImpl implements Controller {

	private String toPrint;
	List<String> printedStrings;

	public ControllerImpl() {
		this.printedStrings = new ArrayList<String>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setToPrint(String str) throws IllegalArgumentException {
		try {
			this.toPrint = Objects.requireNonNull(str, "The string to be printed cannot be null");
		} catch (NullPointerException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getToPrint() {
		return this.toPrint;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getHistory() {
		return this.printedStrings;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void print() throws IllegalStateException {
		if (this.toPrint == null) {
			throw new IllegalStateException("The string to be printed cannot be unset.");
		} else {
			System.out.println(this.toPrint);
			this.printedStrings.add(this.toPrint);
		}
	}

}
